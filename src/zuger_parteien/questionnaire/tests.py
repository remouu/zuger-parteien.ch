#import tests, urls and models
from django.test import TestCase, Client
from django.urls import reverse
from .models import Category, Party, Question, Answer

### tests have to be run with: python manage.py test --keepdb ###

#tests view
class TestViews(TestCase):
    #setup
    def setUp(self):
        self.client = Client()
        self.questionnaire_url = reverse('questionnaire')
        self.num = len(Question.objects.all())
        self.parties = { party.abbr:[None]*self.num for party in Party.objects.all() }
        for answer in Answer.objects.all():
            self.parties[answer.party.abbr][answer.question.id-1] = answer.answer  

    #tests GET request for questioaire
    def test_questionnaire_GET(self):
        #define response
        response  = self.client.get(self.questionnaire_url)

        #asserts stauts code and template
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, "questioaire.html")
    
    #tests for every party 100%
    def test_questionnaire_party_POST(self):
        #for loop for all parties
        for party in list(Party.objects.all()):
            #prepares request
            form = {'form-TOTAL_FORMS': str(self.num), 'form-INITIAL_FORMS': str(self.num), 'form-MIN_NUM_FORMS': '0', 'form-MAX_NUM_FORMS': '1000'}
            answers = { 'form-{}-radio_buttons'.format(id) : str(answer) for answer, id in zip(self.parties[party.abbr], range(self.num))}
            request = dict(list(form.items()) + list(answers.items()))

            #define response
            response = self.client.post(self.questionnaire_url, request)

            #asserts 100% and Template
            self.assertEqual(response.context[-1]["data"][party.abbr], "100.0%")
            self.assertTemplateUsed(response, "results.html")
    
    #tests for 'no answer' for every question
    def test_questionnaire_POST_no_input(self):
        #prepares request
        form = {'form-TOTAL_FORMS': str(self.num), 'form-INITIAL_FORMS': str(self.num), 'form-MIN_NUM_FORMS': '0', 'form-MAX_NUM_FORMS': '1000'}
        answers = { 'form-{}-radio_buttons'.format(id) : 'X' for  id in range(self.num) }
        request = dict(list(form.items()) + list(answers.items()))

        #define response
        response = self.client.post(self.questionnaire_url, request)

        #asserts empty and Template
        self.assertEqual(response.context["empty"], True)
        self.assertTemplateUsed(response, "questioaire.html")