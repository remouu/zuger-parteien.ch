from django.contrib import admin

#import models
from .models import Question, Party, Answer, Category

#register models in admin
admin.site.register(Question)
admin.site.register(Party)
admin.site.register(Answer)
admin.site.register(Category)