from django import forms
from django.forms import formset_factory

#define choices
CHOICES = [
    (0, 'Ja'),
    (25, 'Eher Ja'),
    (75, 'Eher Nein'),
    (100, 'Nein'),
    ('X', 'Keine Antwort')]

#create form
class QuestionForm(forms.Form):
    #create radio buttons
    radio_buttons = forms.ChoiceField(choices=CHOICES, widget=forms.RadioSelect, label="")

#create formset
QuestionFormset = formset_factory(QuestionForm, extra=0)