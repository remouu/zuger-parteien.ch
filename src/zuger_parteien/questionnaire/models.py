from django.db import models

#Categories
class Category(models.Model):
    category_de = models.CharField(max_length = 30)

    #different plural
    class Meta:
        verbose_name_plural = "Categories"

    def __str__(self):
        return str(self.category_de)

#parties
class Party(models.Model):
    abbr = models.CharField(max_length = 5) #party abbreviation
    name = models.CharField(max_length = 40)

    #different plural
    class Meta:
        verbose_name_plural = "Parties"

    def __str__(self):
        return self.abbr

#questions
class Question(models.Model):
    category = models.ForeignKey(Category, on_delete = models.CASCADE)
    question_nr = models.IntegerField(default=1)
    question_de = models.TextField()

    def __str__(self):
        return str(self.question_de)

#party answers
class Answer(models.Model):
    party = models.ForeignKey(Party, on_delete = models.CASCADE)
    question = models.ForeignKey(Question, on_delete = models.CASCADE)
    answer = models.IntegerField()

    def __str__(self):
        return self.party.abbr + " " + str(self.question.id)
