from django.shortcuts import render
from .models import Question, Party, Answer, Category
from .forms import QuestionFormset
from django.views.generic.edit import FormView

#define number of questions
num = len(Question.objects.all())

class QuestionnaireView(FormView):
    #template
    template_name = "questionnaire.html"
    form_class = QuestionFormset

    #define context
    def get_context_data(self, **kwargs):
        context = super(QuestionnaireView, self).get_context_data(**kwargs)
        if self.request.POST:
            context["formset"] = QuestionFormset(self.request.POST)
        else:
            context["formset"] = QuestionFormset(initial = [{'radio_buttons' : 'X'}]*num) #initial data
        context["questions"] = list(Question.objects.all()) #all questions
        context["categories"] = list(Category.objects.all()) #all caegories
        context["data"] = None
        context["keys"] = None
        context["empty"] = False
        return context

    def form_valid(self, form):
        user_answers = [list(answer.values())[0] for answer in form.cleaned_data]
        if user_answers.count('X') == num: #checks for 0 answers
            context = self.get_context_data()
            context["empty"] = True
            return render(self.request, "questionnaire.html", context) #reload questionnaire page
        else:
            data, keys = self.calculate_agreement(user_answers)
            context = {}
            context["test"] = "test"
            context["data"] = data
            context["keys"] = keys
            context["parties"] = { Party.objects.get(id=i).abbr : Party.objects.get(id=i).name for i in range(1, len(Party.objects.all()) +1)}
            return render(self.request, 'results.html', context) #render result page

    #get party answers from db
    def get_party_answers(self):
        parties = { party.abbr:[None]*num for party in Party.objects.all() }
        for answer in Answer.objects.all():
            parties[answer.party.abbr][answer.question.id-1] = answer.answer
        return parties

    def calculate_agreement(self, user_answers):
        parties = self.get_party_answers()
        #calculate agreement
        parties_results = {
            party:
            round(100-sum([abs(int(user_answers[i]) - answers[i])
            for i in range(len(answers))
            if user_answers[i] != 'X'])
            /(len(answers)-user_answers.count('X')), 1)
            for party, answers in parties.items()
            }
        #define sort key
        sort_key = sorted(parties_results, key=lambda key: parties_results[key], reverse=True)
        #make agreement into percent
        for key in parties_results.keys():
            parties_results[key] = str(parties_results[key]) + "%"
        return parties_results, sort_key

#simple function view for static home page
def home_view(request, *args, **kwargs):
    return render(request, "home.html", {})