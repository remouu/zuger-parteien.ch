from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
from django.urls import path

#import views
from questionnaire.views import QuestionnaireView, home_view

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', home_view, name='home'),
    path('home/', home_view, name='home'),
    path('questionnaire/', QuestionnaireView.as_view(), name='questionnaire'),
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
